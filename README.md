# Automous Baja Software

This repository is in the early stages of development. 

Dependancies:
nng 

## Files:

 - `due-code` will contain the arduino sketch developed to interface with the connected sensors
 - `urdf` contains the URDF file and meshes used for the model of the California Baja SAE vehicle
 - `estop.py` contains initial code used to test pynng for message passing. Will serve as the node responsible for triggering estop activities
 - `flysky.py` code written to interpret the serial messages generated from the Flysky receiver. Also connects via socket to a host computer running the pybullet simulation
 - `global_settings.py` global settings shared between files
 - `log.py` a central class to log events or messages
 - `sim-testing.ipynb` the iPython notebook used to experiment with the pybullet simulation
 - `sim.py` future home of the simulator once things are sorted and moved out of the notebook
