
import pigpio
import time
import autobaja.global_settings as gs
pi = pigpio.pi()

step_pin = gs.STEERING_STEP_PIN
dir_pin = gs.STEERING_DIR_PIN
en_pin = gs.STEERING_EN_PIN

pi.set_mode(step_pin, pigpio.OUTPUT)
pi.set_mode(dir_pin, pigpio.OUTPUT)
pi.set_mode(en_pin, pigpio.OUTPUT)




def enable():
    pi.write(en_pin, 0)

def disable():
    pi.write(en_pin, 1)
def left():
    pi.write(dir_pin, 1)
def right():
    pi.write(dir_pin, 0)

def disconnect():
    pi.stop()

enable()

one_rot = 4220 
#delay = 0.00005 
delay = 0.0005 

while True:
    inpt = input("Enter number of steps (right negative, q to quit): ")
    if "q" in str(inpt):
        break
    else:
        inpt = int(inpt)
    if inpt > 0:
        left()
    else:
        right()
    for i in range(abs(inpt)):
        time.sleep(delay)
        pi.write(step_pin, 1)
        time.sleep(delay)
        pi.write(step_pin, 0)

disconnect()
