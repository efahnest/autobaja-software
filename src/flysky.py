# TODO: convert to read whole byte buffer at once, not one at a time
#       - set a loop termination condition
#       - integrate nng 
#


# channel 1 - x axis right trigger, auto returns to 1500
# channel 2 - y axis right trigger, auto returns to 1500
# channel 3 - y axis left trigger, no auto return
# channel 4 - x axis left trigger, auto returns to 1500
# channel 5 - AUX channel 1 - SWD
# channel 6 - AUX channel 2 - VRB

import os, sys
from serial import Serial

import RPi.GPIO as gp #allows access to GPIO pins
import time
import socket
import argparse
from enum import Enum
from datetime import datetime
from autobaja.log import Log
import autobaja.global_settings as gs
import rospy
from std_msgs.msg import Int16MultiArray as int_arr

rospy.init_node("flysky_node")

pub = rospy.Publisher('flysky_data', int_arr, queue_size=10)


class ControllerState(Enum):
    NO_CONNECTION = 1
    ARMING = 2
    ACTIVE = 3
    ESTOP_ACTIVE = 4
    SHUTDOWN_ACTIVE= 5

NO_VALID_RX_TIMEOUT = 3 # seconds between valid messages before going into estop node
RIGHT_TRIGGER_ESTOP_ZONE = 500 # zone that both ch1,ch2 must be out of to not trigger estop mode


state = ControllerState.NO_CONNECTION
log = Log('controller-node', print_as_well=True)



#ld = LogData("serial_" + os.path.split(args.name)[-1]) #create instace of logData class for hf logging

ser = Serial(gs.FLYSKY_SERIAL_PATH, gs.FLYSKY_BAUDRATE, timeout = 5) # 5 second timeout

barr = [] # array to hold bytes read from the serial port 
last = b'\x00'
last_rx_time = datetime.now().timestamp()


def TRIGGER_ESTOP():
    global state
    state = ControllerState.ESTOP_ACTIVE
    log.log("ESTOP has been activated")

def NOTIFY_READY():
    pass

def SEND_CHANNELS(channel_vals):
    msg = int_arr()
    msg.data = channel_vals
    pub.publish(msg)
    log.log("Linear velocity: {}mph, Steering angle: {}deg".format((channel_vals[2]-1000)/1000 * 15, (channel_vals[3]-1500)/500 * 45))
    pass

def TRIGGER_SHUTDOWN():
#    s.sendall(b'shutdown')
    global state
    state = ControllerState.SHUTDOWN_ACTIVE
    log.log("Controller has requested SHUTDOWN state")
    pass

#HOST = '192.168.1.25'
#PORT=50007
#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.connect((HOST,PORT))

while not rospy.is_shutdown():
    b = ser.read(1)
    barr.append(b)
    if (datetime.now().timestamp() - last_rx_time > NO_VALID_RX_TIMEOUT) and state == ControllerState.ACTIVE:
        log.log("NO_VALID_RX_TIMEOUT time exceeded, triggering E-stop")
        TRIGGER_ESTOP()

    if b == b'@' and last == b' ':
        #print(barr)
        if len(barr) < 32:
            log.log("Got pattern without completing message.. continuing")
            continue
        if len(barr) > 32:
            log.log("Message is too long.., resetting array")
            barr = []
            continue
        checksum = int.from_bytes(barr[-1], byteorder='little') + int.from_bytes(barr[-2], byteorder='little')
        for i in range(0,len(barr)-4):
            checksum += int.from_bytes(barr[i], byteorder='little')

        cksum_val = int.from_bytes(barr[-4]+barr[-3], byteorder='little')

        message_value = cksum_val + checksum
        if message_value != 0xffff:
            log.log("THE MESSAGE HAS AN INVALID CHECKSUM, DROPPING")
            continue

        rm_headers_checksum = barr[:-6] # remove the headers
        used_channels = barr[:-20] # remove the unused channels 6-13

        barr = [] # clear barr array
        last_rx_time = datetime.now().timestamp() # update last valid received time

        if state == ControllerState.NO_CONNECTION:
            state = ControllerState.ARMING
            log.log("Controller connection achieved, currently arming")

        # split channels 0-5 into their respective spots in the array
        channel_readings = []
        for i in range(6):
            channel_readings.append(int.from_bytes(used_channels[2*i+1] + used_channels[2*i], byteorder='big'))

        valid_running_state = abs(channel_readings[0]-1500) >= RIGHT_TRIGGER_ESTOP_ZONE or abs(channel_readings[1]-1500) >= RIGHT_TRIGGER_ESTOP_ZONE

       # if shutdown switch is triggered
        if (channel_readings[4] == 2000): #IF SHUTDOWN HAS BEEN REQUESTED
          TRIGGER_SHUTDOWN()
          break

        if state==ControllerState.ARMING or state==ControllerState.ESTOP_ACTIVE:
          if valid_running_state:
            state = ControllerState.ACTIVE
            log.log("Deadman Switch is active. Transitioning to active state and notifying core")
            NOTIFY_READY()

        # if the right trigger deadman switch is not being held while the controller is active...
        if state==ControllerState.ACTIVE:
            if not valid_running_state:
                TRIGGER_ESTOP()
                print(state)
            else:
                SEND_CHANNELS(channel_readings)

#        log.log(str(channel_readings))
    last=b # move current byte to last byte variable


ser.close()
