TEENSY_BAUDRATE = 115200

# Parameters related to the steering stepper motor

STEERING_STEP_PIN = 5 #PU, GPIO 5 pin
STEERING_DIR_PIN = 6 #DR+, GPIO 6 pin
STEERING_EN_PIN = 12 #MF+, GIPO 12 pin

MIN_STEP_PULSE_WIDTH = 0.1e-5 #2.5us
STEERING_STEPS_PER_REVOLUTION = 4225

MAX_STEPS = 1600 # number of steps from center to full left or right, 1750 should work ok without encoder

# Parameters related to the Flysky receiver

FLYSKY_SERIAL_PATH = '/dev/serial0'
FLYSKY_BAUDRATE = 115200

# Parameters related to the accelerator control

ACC_ENABLE_PIN = 19 #to relay/mosfet, GPIO19
ACC_SIGNAL_PWM_PIN = 13 # GPIO13, pwm signal for servo motor

#MIN_PULSEWIDTH = 
#MAX_PULSEWIDTH = 

# Parameters related to the estop system

BOX_ESTOP_PIN = 16 # GPIO 16, active low



