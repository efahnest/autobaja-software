"""
Class used to write all data to datafiles. 

log -> pass in string to write to file, no newline character included
"""
import os
import time
import glob

class Log:
  """
  Constructor, pass in the sensor name as the folder name (ex "adc" for the ADC). Creates the log file 
  """
  def __init__(self, folder_name, print_as_well = False, base_dir="/home/pi/data/"):
    if not os.path.exists(os.path.join(base_dir, folder_name)):
      os.makedirs(os.path.join(base_dir, folder_name)) # if dir does not exist, create..
    num_files = len(glob.glob(os.path.join(base_dir,folder_name, "*.txt")))
    self.file = open(os.path.join(base_dir, folder_name, "{2}-{0}_{1}.txt".format(folder_name,time.strftime("%Y_%m_%d-%H:%M:%S"), str(num_files))), 'w') # open a log file
    self.print_as_well = print_as_well

  """
  Desctructor, flushes and closes the file
  """
  def __del__(self):
    self.file.flush();
    self.file.close();

  """
  String function, returns the current log file
  """
  def __str__(self):
    return self.file.name

  """
  log function, writes a line to the file
  """
  def log(self, toWrite):
    if self.print_as_well:
      print(time.strftime("%Y_%m_%d-%H:%M:%S\t") + toWrite)
    self.file.write(time.strftime("%Y_%m_%d-%H:%M:%S\t") + toWrite + "\n")
    self.file.flush()