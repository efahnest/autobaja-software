from log import Log
import pynng, asyncio

import global_settings as gs

sub = pynng.Sub0(listen=gs.NNG_URL, recv_max_size=100,recv_buffer_size=20, name='estop-node')

sub.subscribe(b'estop')

# async def handle_estop():
  # print(await sub.recv())

# asyncio.run(handle_estop())

while True:
  msg = sub.recv().decode('utf-8').split(':')[1] # get the message
  if msg == "stop":
    print("got stop")
  elif msg == "shutdown":
    print("got shutdown")
  elif msg == "disengage":
    print("got disengage")
sub.close()