import autobaja.global_settings as gs
import pigpio
import time, enum

pi = pigpio.pi()

import rospy
from std_msgs.msg import Int16MultiArray as int_arr_msg
from std_msgs.msg import Int32, Float64

rospy.init_node("steering_node")

controller_state = []
encoder_state = -5000

def control_callback(msg):
  global controller_state
  controller_state = msg.data
  return

def encoder_callback(msg):
  global encoder_state
  encoder_state = msg.data
  return

control_sub = rospy.Subscriber("/flysky_data", int_arr_msg, control_callback)
encoder_sub = rospy.Subscriber("/lin_pot_pin", Int32, encoder_callback)

step_pin = gs.STEERING_STEP_PIN
dir_pin = gs.STEERING_DIR_PIN
en_pin = gs.STEERING_EN_PIN

pi.set_mode(step_pin, pigpio.OUTPUT)
pi.set_mode(dir_pin, pigpio.OUTPUT)
pi.set_mode(en_pin, pigpio.OUTPUT)

class stepperState(enum.Enum):
  disabled=1
  enabled=2
  left=3
  right=4
state = stepperState.disabled

def enable():
  global state
  state = stepperState.enabled
  pi.write(en_pin, 0)

def disable():
  global state
  state = stepperState.disabled
  pi.write(en_pin, 1)

def left():
  global state
  state = stepperState.left
  pi.write(dir_pin, 1)

def right():
  global state
  state = stepperState.right
  pi.write(dir_pin, 0)

def disconnect():
  disable()
  pi.stop()

def step():
  global state, position
  if state == stepperState.enabled or state == stepperState.disabled:
    print("Must set the stepper to left or right before calling step")
    return
  if abs(position) >= abs(gs.MAX_STEPS):
    print("At max steps, skipping..")
    return
  pi.write(step_pin, 1)
  time.sleep(gs.MIN_STEP_PULSE_WIDTH)
  pi.write(step_pin, 0)
  if state == stepperState.left:
    position -= 1
  elif state == stepperState.right:
    position += 1


rate = rospy.Rate(1000)
enable()
iteration = 0 
start_time = time.time()

time.sleep(1)

position = 0

while not rospy.is_shutdown():
#  iteration+= 1 
#  if iteration % 1000 == 0:
#    dt = time.time() - start_time
#    iteration = 0
#    print(f"running rate: {1000 / dt} hz")
#    start_time = time.time()
  if len(controller_state)==0:
    continue
  control_signal = controller_state[3]
  control_signal -= 1500 # centered, will range from -500 to 500
  control_signal *= 2 #\pm 1000 
  if position < control_signal:
    right()
  else:
    left()

  if position != control_signal:
    step()
  #print(f"pos: {position} control: {control_signal}")

#  rate.sleep()

disconnect()
