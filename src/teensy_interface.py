
import os, sys
from serial import Serial

import RPi.GPIO as gp #allows access to GPIO pins
import time, os, glob
import socket
import argparse
from enum import Enum
from datetime import datetime
from autobaja.log import Log
import autobaja.global_settings as gs
import rospy

rospy.init_node('teensy_publisher', anonymous=True)

from std_msgs.msg import String, Float64, Int32

ser_options = glob.glob("/dev/serial/by-id/*")

teensy_path = ""
for i in ser_options:
    if "Teensy" in i:
        print("Found Teensy serial port: " + i)
        teensy_path = i
        break

if teensy_path == "":
    print("ERROR: TEENSY NOT FOUND. Got " + str(ser_options) + " for serial options")
    assert 1==2

ser = Serial(teensy_path, gs.TEENSY_BAUDRATE, 5)
print("got ser open!")

### Create publishers


publishers = {}
rec_successful_message = False
while not rec_successful_message and not rospy.is_shutdown():
    ser.flushInput()
    data = ser.readline().decode("utf-8")[:-2] # trim newline
    if len(data) < 4: # min message length is ~||~, 4
        print("Received message shorter than 4 characters, discarding")
        continue
    if data[:2] != '~|' or data[-2:] != '|~':
        print(f"Discarding message {data} because of lacking start/end bits\n")
        continue
    data = data.split("|")[1:-1] # trim off start and end characters
    
    for datapoint in data:
        channel = datapoint.split(":")[0]
        data = datapoint.split(":")[1]
        if "." in data:
            msg_type = Float64
        else:
            msg_type = Int32

        publishers[channel] = rospy.Publisher(channel, msg_type, queue_size=10)
    rec_successful_message = True    



rate = rospy.Rate(240)
iteration = 0 
start_time = time.time()
print("Starting lower loop")
try:
    while not rospy.is_shutdown():
        iteration+= 1 
        if iteration % 100 == 0:
            dt = time.time() - start_time
            iteration = 0
            print(f"running rate: {100 / dt} hz")
            start_time = time.time()

        ser.flushInput()
        data = ser.readline().decode("utf-8")[:-2] # trim newline
        if len(data) < 4: # min message length is ~||~, 4
            print("Received message shorter than 4 characters, discarding")
            continue
        if data[:2] != '~|' or data[-2:] != '|~':
            print(f"Discarding message {data} because of lacking start/end bits\n")
            continue
        data = data.split("|")[1:-1] # trim off start and end characters
        for datapoint in data:
            #print("datapoint is", datapoint)
            channel = datapoint.split(":")[0]
            value = datapoint.split(":")[1]
            if "." in value:
                value = float(value)
            else:
                value = int(value)

            publishers[channel].publish(value)
        rate.sleep()

except rospy.ROSInterruptException:
    pass
ser.close()

