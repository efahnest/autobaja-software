import autobaja.global_settings as gs
import pigpio
import time, enum

pi = pigpio.pi()

import rospy
from std_msgs.msg import Int16MultiArray as int_arr_msg
from std_msgs.msg import Int32, Float64

rospy.init_node("steering_node")

controller_state = []
encoder_state = -5000

def control_callback(msg):
  global controller_state
  controller_state = msg.data
  return


control_sub = rospy.Subscriber("/flysky_data", int_arr_msg, control_callback)

control_pin = gs.ACC_SIGNAL_PWM_PIN
en_pin = gs.ACC_ENABLE_PIN
print(control_pin, en_pin)
pi.set_mode(en_pin, pigpio.OUTPUT)
pi.set_mode(control_pin, pigpio.OUTPUT)

class servoState(enum.Enum):
  disabled=1
  enabled=2
state = servoState.disabled

def enable():
  global state
  state = servoState.enabled
  pi.write(en_pin, 1)

def disable():
  global state
  state = servoState.disabled
  pi.write(en_pin, 0)

def set_pos(position):
  global state
  pi.set_servo_pulsewidth(control_pin, position)

def disconnect():
  disable()
  pi.stop()



rate = rospy.Rate(1000)
iteration = 0 
start_time = time.time()

time.sleep(1)

position = 0

while not rospy.is_shutdown():
  while True:
    val= input("Enter position: ")
    if "q" in val:
      print("Exiting")
      disable()
      break
    elif "e" in val:
      print("Enabling")
      enable()
    elif "d" in val:
      print("Disabling")
      disable()
    else:
      val = int(val)
      print(f"Setting position to {val}")
      set_pos(val)

  #print(f"pos: {position} control: {control_signal}")

#  rate.sleep()

disconnect()
